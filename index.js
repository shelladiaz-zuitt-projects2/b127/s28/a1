/**
 * 1. In the S28 folder, create an activity folder and an index.html and a script.js file inside of it.
2. Link the index.js file to the index.html file. name the title tag "Postman and REST API Activity"
//API placeholder that you will use:
//https://jsonplaceholder.typicode.com/todo
19. Export the Postman collection and save it in the activity folder.
20. Create a git repository named S28.
21. Initialize a local git repository, add the remote link and push to git with the commit message of s28 activity.
22. Add the link in Boodle named 28 Introduction to Postman and REST

SECTIONS that I need to see

1. Getting all to do list item
2. Getting a specific to do list item
3. Creating a to do list item using POST method
4. Updating a to do list item using PUT method
5. Updating a to do list item using PATCH method
6. Deleting a to do list item
 */



//https://jsonplaceholder.typicode.com/todos

//3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
// fetch('https://jsonplaceholder.typicode.com/todos')
// .then(response => response.json())
// .then(json => console.log(json))

//4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
// fetch('https://jsonplaceholder.typicode.com/todos')
// .then(res => res.json())
// .then(data => {
//     let list = data.map(post => {
//         return post.title
//     })
//     console.log(list)
// })

//5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
// fetch('https://jsonplaceholder.typicode.com/todos/1')
// .then(response => response.json())
// .then(data => console.log(data))

//6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
// fetch('https://jsonplaceholder.typicode.com/todos/1')
// .then(res => res.json())
// .then(json => console.log(json.title, json.completed))

//7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
// fetch('https://jsonplaceholder.typicode.com/todos',{
//     method: 'POST',
//     headers: {
//         'Content-Type': 'application/json'
//     },
//     body: JSON.stringify({
//         userId: 1,
//         title: 'Sample Post',
//         completed: false
//     })
// })
// .then( res => res.json())
// .then( data => console.log(data))

//8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
// fetch('https://jsonplaceholder.typicode.com/todos/1',{
//     method: 'PUT',
//     headers: {
//         'Content-Type': 'application/json'
//     },
//     body: JSON.stringify({
//         userId: 1,
//         title: 'Modified Sample Post',
//         completed: true
//     })
// })
// .then( res => res.json())
// .then( data => console.log(data))

// 9. Update a to do list item by changing the data structure to contain the following properties:
// - Title
// - Description
// - Status
// - Date Completed
// - User ID
// fetch('https://jsonplaceholder.typicode.com/todos/1',{
//     method: 'PUT',
//     headers: {
//         'Content-Type': 'application/json'
//     },
//     body: JSON.stringify({
//         userId: 1,
//         title: 'Changed Blog Post',
//         completed: true,
//         description: 'my first blog',
//         date: 'August 23, 2021'
//     })
// })
// .then( res => res.json())
// .then( data => console.log(data))

//10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
// fetch('https://jsonplaceholder.typicode.com/todos/1',{
//     method: 'PATCH',
//     headers: {
//         'Content-Type': 'application/json'
//     },
//     body: JSON.stringify({
//         title: 'NEW Modified Blog Post'
//     })
// })
// .then( res => res.json())
// .then( data => console.log(data))

//11. Update a to do list item by changing the status to complete and add a date when the status was changed.
// fetch('https://jsonplaceholder.typicode.com/todos/1',{
//     method: 'PATCH',
//     headers: {
//         'Content-Type': 'application/json'
//     },
//     body: JSON.stringify({
//         completed: true,
//         date: 'August 25, 2021'
//     })
// })
// .then( res => res.json())
// .then( data => console.log(data))

//12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
// fetch('https://jsonplaceholder.typicode.com/todos/1',{
//     method: 'DELETE'
// })
// .then( res => res.json())
// .then( data => console.log(data))

// 13. Create a request via Postman to retrieve all the to do list items.
// - GET HTTP method
// - https://jsonplaceholder.typicode.com/todos URI endpoint
// - Save this request as get all to do list items

// 14. Create a request via Postman to retrieve an individual to do list item.
// - GET HTTP method
// - https://jsonplaceholder.typicode.com/todos/1 URI endpoint
// - Save this request as get to do list item

// 15. Create a request via Postman to create a to do list item.
// - POST HTTP method
// - https://jsonplaceholder.typicode.com/todos URI endpoint
// - Save this request as create to do list item

// 16. Create a request via Postman to update a to do list item.
// - PUT HTTP method
// - https://jsonplaceholder.typicode.com/todos/1 URI endpoint
// - Save this request as update to do list item PUT
// - Update the to do list item to mirror the data structure used in the PUT fetch request

// 17. Create a request via Postman to update a to do list item.
// - PATCH HTTP method
// - https://jsonplaceholder.typicode.com/todos/1 URI endpoint
// - Save this request as create to do list item
// - Update the to do list item to mirror the data structure of the PATCH fetch request

// 18. Create a request via Postman to delete a to do list item.
// - DELETE HTTP method
// - https://jsonplaceholder.typicode.com/todos/1 URI endpoint
// - Save this request as delete to do list item


